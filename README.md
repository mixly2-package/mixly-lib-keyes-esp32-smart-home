## Mixly2.0第三方库模板

1.安装依赖

```bash
npm install
```

2.打包

- 针对开发环境打包

```bash
npm run build:dev
```

- 针对生产环境打包

```bash
npm run build:prod
```


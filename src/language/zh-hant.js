export const ZhHantMsg = {};

ZhHantMsg.ke_LED = 'LED灯';
ZhHantMsg.MIXLY_ANALOGWRITE_PIN = '類比輸出裝置  @腳位';
ZhHantMsg.MIXLY_VALUE2 = '賦值為';
ZhHantMsg.Kids_ON = '高';
ZhHantMsg.Kids_OFF = '低';
ZhHantMsg.Kids_anologWrite = 'PWM模拟输出';

ZhHantMsg.Kids_iic = '管脚：SDA# A4, SCL# A5';
ZhHantMsg.Kids_rot = '按钮管脚';
ZhHantMsg.Kids_rot_count = '计数变量';

ZhHantMsg.Kids_bits = '字符串';
ZhHantMsg.Kids_pin = '管脚';

ZhHantMsg.Kids_iic_pin = '管脚SDA:A4,SCL:A5';
ZhHantMsg.Kids_lcd_p = '液晶显示屏';
ZhHantMsg.Kids_shilihua = '实例化名称';
ZhHantMsg.Kids_size = '字体大小';
ZhHantMsg.Kids_printcount = '显示数字';
ZhHantMsg.ke_string = '显示字符';

ZhHantMsg.Kids_lcd_left = '液晶显示屏往左滚动';
ZhHantMsg.Kids_lcd_right = '液晶显示屏往右滚动';

ZhHantMsg.ke_TM1637 = '4位8段数码管';
ZhHantMsg.ke_ws = '位数';
ZhHantMsg.ke_begin = '显示的位置';
ZhHantMsg.ke_fill0 = '是否补充0';
ZhHantMsg.ke_light = '亮度0~7';
ZhHantMsg.ke_XY = '显或隐';
ZhHantMsg.ke_L = '左边';
ZhHantMsg.ke_R = '右边';
ZhHantMsg.ke_MH = '冒号';
ZhHantMsg.ke_value = '数值';

ZhHantMsg.MIXLY_WIFI_INIT = 'WIFI 初始化';
ZhHantMsg.MIXLY_WIFI_NAME = '名称';
ZhHantMsg.MIXLY_WIFI_PASSWORD = '密码';
ZhHantMsg.MIXLY_WIFI_READ = 'WIFI 读取数据';
ZhHantMsg.MIXLY_CLIENT_PRINT = 'WIFI 打印';
ZhHantMsg.MIXLY_DHT_PRINT = 'WIFI打印 温湿度脚位';

ZhHantMsg.ke_oled_init = 'OLED初始化';
ZhHantMsg.ke_oled_piexl = 'OLED_画点的坐标';
ZhHantMsg.ke_oled_x = '列';
ZhHantMsg.ke_oled_y = '行';
ZhHantMsg.ke_oled_cong = '从';
ZhHantMsg.ke_oled_dao = '到';
ZhHantMsg.ke_oled_kai = '起始位';
ZhHantMsg.ke_oled_kuan = '宽';
ZhHantMsg.ke_oled_chang = '长';
ZhHantMsg.ke_oled_angle1 = '角度1为';
ZhHantMsg.ke_oled_angle2 = '角度2为';
ZhHantMsg.ke_oled_angle3 = '角度3为';

ZhHantMsg.ke_oled_line = 'OLED_画两点连线';
ZhHantMsg.ke_oled_rect = 'OLED_画空心矩形';
ZhHantMsg.ke_oled_fil_lrect = 'OLED_画实心矩形';
ZhHantMsg.ke_oled_r_rect = 'OLED_画倒圆角的空心矩形';
ZhHantMsg.ke_oled_r_fill_rect = 'OLED_画倒圆角的实心矩形';
ZhHantMsg.ke_oled_circle = 'OLED_画空心圆形  圆心坐标';
ZhHantMsg.ke_oled_circle_radius = '圆半径';
ZhHantMsg.ke_oled_radius = '圆角半径';
ZhHantMsg.ke_oled_fill_circle = 'OLED_画实心的圆形  圆心坐标';
ZhHantMsg.ke_oled_triangle = 'OLED_画空心三角形';
ZhHantMsg.ke_oled_fill_triangle = 'OLED_画实心三角形';
ZhHantMsg.ke_oled_string1 = 'OLED_显示字符串或数字';
ZhHantMsg.ke_oled_weizhi = '显示的位置';
ZhHantMsg.ke_oled_print = '显示';
ZhHantMsg.ke_oled_clear = 'OLED_清屏';


ZhHantMsg.MIXLY_ke_LED1 = '食人魚LED';
ZhHantMsg.MIXLY_ke_LED2 = '紅色食人魚LED';
ZhHantMsg.MIXLY_ke_LED3 = '綠色食人魚LED';
ZhHantMsg.MIXLY_ke_LED4 = '黃色食人魚LED';
ZhHantMsg.MIXLY_ke_LED5 = '藍色食人魚LED';
ZhHantMsg.MIXLY_ke_LED01 = '草帽LED';
ZhHantMsg.MIXLY_ke_LED02 = '紅色草帽LED';
ZhHantMsg.MIXLY_ke_LED03 = '綠色草帽LED';
ZhHantMsg.MIXLY_ke_LED04 = '黃色草帽LED';
ZhHantMsg.MIXLY_ke_LED05 = '藍色草帽LED';
ZhHantMsg.MIXLY_ke_QCD = '七彩led灯';
ZhHantMsg.MIXLY_ke_RGB_A = '共阳RGB';
ZhHantMsg.MIXLY_ke_RGB_B = '共阴RGB';

ZhHantMsg.MIXLY_ke_BUZZER1 = '有源蜂鳴器';
ZhHantMsg.MIXLY_ke_BUZZER2 = '無源蜂鳴器';
ZhHantMsg.MIXLY_ke_RELAY = '繼電器';
ZhHantMsg.MIXLY_ke_v_motor = '震动马达';
ZhHantMsg.MIXLY_ke_SPEAKER = '扬声器';
ZhHantMsg.MIXLY_NOTONE_PIN = '结束声音 管脚#';
ZhHantMsg.MIXLY_ke_MOTOR = '风扇';
ZhHantMsg.MIXLY_MOTOR_ANALOG = '模拟输出值';
ZhHantMsg.MIXLY_ke_MOTOR01 = '减速电机';
ZhHantMsg.MIXLY_ke_SERVO = '舵機';
ZhHantMsg.MIXLY_ke_TB6612 = 'TB6612电机驱动';
ZhHantMsg.MIXLY_H = '正';
ZhHantMsg.MIXLY_L = '反';


ZhHantMsg.MIXLY_RGB_INIT = 'RGB燈初始化';
ZhHantMsg.MIXLY_RGB_SET_BRIGHTNESS = 'RGB燈設置亮度';
ZhHantMsg.MIXLY_RGB_SET_COLOR = 'RGB燈設置顏色';
ZhHantMsg.MIXLY_RGB_SHOW = 'RGB燈設置生效';
ZhHantMsg.MIXLY_RGB = 'RGB燈';
ZhHantMsg.MIXLY_CHASE = '跑馬燈';
ZhHantMsg.MIXLY_RAINBOW = '彩虹';
ZhHantMsg.MIXLY_RGB_NUM = '燈號';
ZhHantMsg.MIXLY_RGB_COUNT = '燈數';
ZhHantMsg.MIXLY_RGB_R = 'R值';
ZhHantMsg.MIXLY_RGB_G = 'G值';
ZhHantMsg.MIXLY_RGB_B = 'B值';
ZhHantMsg.MIXLY_RGBdisplay_rgb_rainbow1 = '七彩變換切換時間';
ZhHantMsg.MIXLY_RGBdisplay_rgb_rainbow2 = '七彩迴圈切換時間';
ZhHantMsg.MIXLY_RGB_DISPLAY_RAINBOW_TYPE_1 = '普通';
ZhHantMsg.MIXLY_RGB_DISPLAY_RAINBOW_TYPE_2 = '漸變';
ZhHantMsg.MIXLY_RGB_display_rgb_rainbow3 = '彩虹值';

ZhHantMsg.MIXLY_ke_IR_G = '人體紅外熱傳感器';
ZhHantMsg.MIXLY_ke_FLAME = '火焰傳感器';
ZhHantMsg.MIXLY_ke_HALL = '霍爾傳感器';
ZhHantMsg.MIXLY_ke_CRASH = '碰撞傳感器';
ZhHantMsg.MIXLY_ke_BUTTON = '按鍵';
ZhHantMsg.MIXLY_ke_sl_BUTTON = '自锁按键';
ZhHantMsg.MIXLY_ke_TUOCH = '電容觸摸傳感器';
ZhHantMsg.MIXLY_ke_KNOCK = '敲擊傳感器';
ZhHantMsg.MIXLY_ke_TILT = '傾斜傳感器';
ZhHantMsg.MIXLY_ke_SHAKE = '振動傳感器';
ZhHantMsg.MIXLY_ke_REED_S = '幹簧管傳感器';
ZhHantMsg.MIXLY_ke_TRACK = '循跡傳感器';
ZhHantMsg.MIXLY_ke_AVOID = '避障傳感器';
ZhHantMsg.MIXLY_ke_LIGHT_B = '光折斷傳感器';
ZhHantMsg.MIXLY_ke_ROT = '旋转编码器';

ZhHantMsg.MIXLY_ke_ANALOG_T = '模擬溫度傳感器';
ZhHantMsg.MIXLY_ke_SOUND = '聲音傳感器';
ZhHantMsg.MIXLY_ke_LIGHT = '光敏傳感器';
ZhHantMsg.MIXLY_ke_UV = '紫外线传感器';
ZhHantMsg.MIXLY_ke_Piezo = '陶瓷压电传感器';
ZhHantMsg.MIXLY_ke_WATER = '水位傳感器';
ZhHantMsg.MIXLY_ke_SOIL = '土壤傳感器';
ZhHantMsg.MIXLY_ke_POTENTIOMETER = '旋转電位器';
ZhHantMsg.MIXLY_ke_LM35 = 'LM35溫度傳感器';
ZhHantMsg.MIXLY_ke_SLIDE_POTENTIOMETER = '滑動電位器';
ZhHantMsg.MIXLY_ke_TEMT6000 = 'TEMT6000環境光傳感器';
ZhHantMsg.MIXLY_ke_STEAM = '水蒸氣傳感器';
ZhHantMsg.MIXLY_ke_FILM_P = '薄膜壓力傳感器';
ZhHantMsg.MIXLY_ke_JOYSTICK = '遙桿傳感器';
ZhHantMsg.MIXLY_ke_JOYSTICK_btn = '遥杆按钮';

ZhHantMsg.MIXLY_ke_SMOKE_DATA = '煙霧傳感器 数字';
ZhHantMsg.MIXLY_ke_SMOKE_ANALOG = '煙霧傳感器 模拟';
ZhHantMsg.MIXLY_ke_ALCOHOL = '酒精傳感器';
ZhHantMsg.MIXLY_ke_Voltage = '电压传感器';
ZhHantMsg.MIXLY_ke_Current = '电流传感器';


ZhHantMsg.MIXLY_ke_18B20 = '18B20溫度傳感器';
ZhHantMsg.MIXLY_ke_18B20_R = '获取温度';
ZhHantMsg.MIXLY_ke_DHT11 = '溫濕度傳感器';
ZhHantMsg.MIXLY_DHT11_H = '获取湿度';    /////////////
ZhHantMsg.MIXLY_DHT11_T = '获取温度';     ////////////
ZhHantMsg.MIXLY_ke_BMP180 = 'BMP180高度計傳感器';
ZhHantMsg.MIXLY_ke_BMP180_T = '温度';
ZhHantMsg.MIXLY_ke_BMP180_A = '大气压';
ZhHantMsg.MIXLY_ke_BMP180_H = '海拔高度';

ZhHantMsg.MIXLY_ke_BMP280 = 'BMP280高度計傳感器';
ZhHantMsg.MIXLY_ke_BMP280_T = '温度';
ZhHantMsg.MIXLY_ke_BMP280_A = '大气压';
ZhHantMsg.MIXLY_ke_BMP280_H = '海拔高度';

ZhHantMsg.MIXLY_ke_SR04 = 'SR04超聲波模塊';
ZhHantMsg.MIXLY_ke_3231 = '3231時鐘';

//RTC-DS3231/DS1307
ZhHantMsg.MIXLY_ke_DS3231 = 'DS3231';
ZhHantMsg.MIXLY_ke_DS1307 = 'DS1307';
ZhHantMsg.MIXLY_ke_DS3231_GET_TIME = '獲取';
ZhHantMsg.MIXLY_ke_DS3231_SET_TIME = '設置時間';
ZhHantMsg.MIXLY_ke_DS3231_SET_TIME2 = '編譯的日期與時間';
ZhHantMsg.MIXLY_ke_YEAR = '年';
ZhHantMsg.MIXLY_ke_MONTH = '月';
ZhHantMsg.MIXLY_ke_DAY = '日';
ZhHantMsg.MIXLY_ke_HOUR = '時';
ZhHantMsg.MIXLY_ke_MINUTE = '分';
ZhHantMsg.MIXLY_ke_SECOND = '秒';
ZhHantMsg.MIXLY_ke_DAYOFWEEK = '星期';

ZhHantMsg.MIXLY_ke_ADXL345 = '加速度傳感器';
ZhHantMsg.MIXLY_ADXL345_X = 'X轴加速度'; ///
ZhHantMsg.MIXLY_ADXL345_Y = 'Y轴加速度'; ///
ZhHantMsg.MIXLY_ADXL345_Z = 'Z轴加速度'; ///
ZhHantMsg.MIXLY_ADXL345_XA = 'X轴角度';  ///
ZhHantMsg.MIXLY_ADXL345_YA = 'Y轴角度';  ///
ZhHantMsg.MLX90614_TYPE = '紅外測溫傳感器';
ZhHantMsg.MLX90614_TARGET_OBJECT_TEMP = '目標物體溫度';
ZhHantMsg.MLX90614_AMBIENT_TEMP = '周圍環境溫度';
ZhHantMsg.TCS34725_Get_RGB = 'TCS34725顏色感測器 獲取顏色';
ZhHantMsg.ke_Gesture_APDS = '手势传感器获取手势';


ZhHantMsg.MIXLY_DF_LCD = 'LCD 顯示幕';
ZhHantMsg.MIXLY_LCD_PRINT1 = '列印第1列(row)';
ZhHantMsg.MIXLY_LCD_PRINT2 = '列印第2列(row)';
ZhHantMsg.MIXLY_LCD_PRINT3 = '列印第3列(row)';
ZhHantMsg.MIXLY_LCD_PRINT4 = '列印第4列(row)';
ZhHantMsg.MIXLY_LCD_ROW = '列(row)在第';
ZhHantMsg.MIXLY_LCD_COLUMN = '行(column)在第';
ZhHantMsg.MIXLY_LCD_PRINT = '列列印';
ZhHantMsg.MIXLY_LCD_SETCOLOR = '設置顏色';
ZhHantMsg.MIXLY_LCD_STAT_CURSOR = '有游標';
ZhHantMsg.MIXLY_LCD_STAT_NOCURSOR = '無游標';
ZhHantMsg.MIXLY_LCD_STAT_BLINK = '閃爍';
ZhHantMsg.MIXLY_LCD_STAT_NOBLINK = '不閃爍';
ZhHantMsg.MIXLY_LCD_STAT_CLEAR = '清屏';
ZhHantMsg.MIXLY_LCD_NOBACKLIGHT = '關閉背光';
ZhHantMsg.MIXLY_LCD_BACKLIGHT = '打開背光';
ZhHantMsg.MIXLY_NUMBER = '數字';
ZhHantMsg.MIXLY_ke_MATRIX = '8*8點陣';

ZhHantMsg.MIXLY_LCD128_SETUP = '128X32 LCD 初始化';  ////////////////
ZhHantMsg.MIXLY_LCD128_CURSOR = '128X32 LCD 设置位置';
ZhHantMsg.MIXLY_DISPLAY_STRING = '128X32 LCD 显示字符';
ZhHantMsg.MIXLY_ke_LCD128_PIXLE = '128X32 LCD 画点坐标';
ZhHantMsg.MIXLY_ke_LCD128_D = '128X32 LCD 删除点坐标';
ZhHantMsg.MIXLY_LCD128_CLEAR = '128X32 LCD 清屏';


ZhHantMsg.MIXLY_ke_TM1637 = '4位8段數碼管';
ZhHantMsg.MIXLY_ke_TM1637_C = '位数';
ZhHantMsg.MIXLY_ke_TM1637_P = '在第几位开始显示';
ZhHantMsg.MIXLY_ke_TM1637_Fill = '是否填充0';
ZhHantMsg.MIXLY_ke_TM1637_light = '亮度0~7';
ZhHantMsg.MIXLY_ke_TM1637_xy = '显或隐';
ZhHantMsg.MIXLY_ke_TM1637_left = '左边';
ZhHantMsg.MIXLY_ke_TM1637_maohao = '冒号';
ZhHantMsg.MIXLY_ke_TM1637_right = '右边';
ZhHantMsg.MIXLY_ke_value = '数值';

//4 Display
ZhHantMsg.MIXLY_ke_4DIGITDISPLAY = '四位數碼管';
ZhHantMsg.MIXLY_ke_4DIGITDISPLAY_DISPLAYSTRING = '顯示字符串';
ZhHantMsg.MIXLY_ke_4DIGITDISPLAY_DISPLAYNUMBER = '顯示數字';
ZhHantMsg.MIXLY_ke_4DIGITDISPLAY_NOMBER1 = '第';
ZhHantMsg.MIXLY_ke_4DIGITDISPLAY_NOMBER2 = '個';
ZhHantMsg.MIXLY_ke_4DIGITDISPLAY_DOT = '小數點';
ZhHantMsg.MIXLY_ke_4DIGITDISPLAY_BRIGHTNESS = '亮度';
ZhHantMsg.MIXLY_ke_4DIGITDISPLAY_BRIGHTNESS_RANGE = '(1~8)';
ZhHantMsg.MIXLY_ke_4DIGITDISPLAY_TIME_HOUR = '時';
ZhHantMsg.MIXLY_ke_4DIGITDISPLAY_TIME_MINUTE = '分';
ZhHantMsg.MIXLY_ke_4DIGITDISPLAY_TIME_SECOND = '秒';
//4 Display time
ZhHantMsg.MIXLY_ke_TIME_SEC_ON = '亮';
ZhHantMsg.MIXLY_ke_TIME_SEC_BLINK = '閃';
ZhHantMsg.MIXLY_ke_TIME_SEC_OFF = '滅';

ZhHantMsg.MIXLY_ke_IR_E = '紅外發射模塊';
ZhHantMsg.MIXLY_ke_IR_R = '紅外接收模塊';
ZhHantMsg.MIXLY_ke_W5100 = 'W5100以太網模塊';
ZhHantMsg.MIXLY_rc522_iic_init = 'RFID RC522_I2C 初始化';
ZhHantMsg.MIXLY_rc522_iic_read = 'RFID RC522_I2C 读取值';
ZhHantMsg.MIXLY_ke_BLUETOOTH = '藍牙';
ZhHantMsg.MIXLY_ke_read = '接收到信号';

ZhHantMsg.MIXLY_ke_Count = '灯号';

ZhHantMsg.MIXLY_ke_YEAR = '年';
ZhHantMsg.MIXLY_ke_MONTH = '月';
ZhHantMsg.MIXLY_ke_DAY = '日';
ZhHantMsg.MIXLY_ke_HOUR = '时';
ZhHantMsg.MIXLY_ke_MINUTE = '分';
ZhHantMsg.MIXLY_ke_SECOND = '秒';
ZhHantMsg.MIXLY_ke_WEEK = '周';

ZhHantMsg.MIXLY_ke_angle = '角度';

ZhHantMsg.kids_Ode_to_joy = "圣诞歌";
ZhHantMsg.kids_birthday = "生日快乐";

ZhHantMsg.kids_tone = "音调";
ZhHantMsg.kids_beat = "节拍";
ZhHantMsg.kids_play_tone = "播放乐曲";
ZhHantMsg.kids_notone = "关闭蜂鸣器";

ZhHantMsg.kids_ADkey = "5位按键模块";

export const ZhHantCatgories = {};
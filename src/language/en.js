export const EnMsg = {};


EnMsg.ke_LED = 'led';

EnMsg.MIXLY_ANALOGWRITE_PIN = 'AnalogWrite PIN#';

EnMsg.MIXLY_VALUE2 = 'value';


EnMsg.Kids_ON = 'HIGH';
EnMsg.Kids_OFF = 'LOW';
EnMsg.Kids_anologWrite = 'anologWrite';

EnMsg.Kids_iic = 'PIN：SDA# A4, SCL# A5';
EnMsg.Kids_rot = 'button_PIN';
EnMsg.Kids_rot_count = 'count';
EnMsg.Kids_bits = 'string';
EnMsg.Kids_pin = 'PIN';

EnMsg.Kids_iic_pin = 'PIN #SDA:A4,#SCL:A5';
EnMsg.Kids_lcd_p = 'LCD';
EnMsg.Kids_shilihua = 'Instantiation name';
EnMsg.Kids_size = 'font size';

EnMsg.Kids_printcount = 'Display digits';
EnMsg.ke_string = 'display character';

EnMsg.Kids_lcd_left = 'LCD_Scroll to the left';
EnMsg.Kids_lcd_right = 'LCD_Scroll to the right';

EnMsg.ke_TM1637='4 digit 8-segment LED digital';
EnMsg.ke_ws='digit';
EnMsg.ke_begin='Display position';
EnMsg.ke_fill0='add 0?';
EnMsg.ke_light='Brightness0~7';
EnMsg.ke_XY='Show or hide';
EnMsg.ke_L='left';
EnMsg.ke_R='right';
EnMsg.ke_MH='colon';
EnMsg.ke_value='value';


EnMsg.MIXLY_WIFI_INIT='WIFI ';
EnMsg.MIXLY_WIFI_NAME='Name';
EnMsg.MIXLY_WIFI_PASSWORD='password';
EnMsg.MIXLY_WIFI_READ='WIFI read data';
EnMsg.MIXLY_CLIENT_PRINT='WIFI print';
EnMsg.MIXLY_DHT_PRINT='WIFI print DHT11 pin';

EnMsg.ke_oled_init = 'OLED_init';
EnMsg.ke_oled_piexl = 'OLED_point coordinates';
EnMsg.ke_oled_x = 'column';
EnMsg.ke_oled_y = 'row';
EnMsg.ke_oled_cong = 'from';
EnMsg.ke_oled_dao = 'to';
EnMsg.ke_oled_kai = 'initial point';
EnMsg.ke_oled_kuan = 'width';
EnMsg.ke_oled_chang = 'height';
EnMsg.ke_oled_angle1 = 'angle1';
EnMsg.ke_oled_angle2 = 'angle2';
EnMsg.ke_oled_angle3 = 'angle3';

EnMsg.ke_oled_line = 'OLED_line';
EnMsg.ke_oled_rect = 'OLED_hollow rectangle';
EnMsg.ke_oled_fil_lrect = 'OLED_solid rectangle';
EnMsg.ke_oled_r_rect = 'OLED_hollow rounded rectangle';
EnMsg.ke_oled_r_fill_rect = 'OLED_solid rounded rectangle';
EnMsg.ke_oled_circle = 'OLED_hollow circle  Center coordinates';
EnMsg.ke_oled_circle_radius = 'Circle radius';
EnMsg.ke_oled_radius = 'Corner radius';
EnMsg.ke_oled_fill_circle = 'OLED_solid circle  Center coordinates';
EnMsg.ke_oled_triangle = 'OLED_hollow triangle';
EnMsg.ke_oled_fill_triangle = 'OLED_solid triangle';
EnMsg.ke_oled_string1 = 'OLED_displays a string or number';
EnMsg.ke_oled_weizhi = 'display position';
EnMsg.ke_oled_print = 'display';
EnMsg.ke_oled_clear = 'OLED_clear';


EnMsg.MIXLY_ke_LED1='Piranha LED';
EnMsg.MIXLY_ke_LED2='Red Piranha LED';
EnMsg.MIXLY_ke_LED3='Green Piranha LED';
EnMsg.MIXLY_ke_LED4='Yellow Piranha LED';
EnMsg.MIXLY_ke_LED5='Blue Piranha LED';
EnMsg.MIXLY_ke_LED01='Straw cap LED';
EnMsg.MIXLY_ke_LED02='Red Straw cap LED';
EnMsg.MIXLY_ke_LED03='Green Straw cap LED';
EnMsg.MIXLY_ke_LED04='Yellow straw cap LED';
EnMsg.MIXLY_ke_LED05='Blue Straw cap LED';
EnMsg.MIXLY_ke_QCD='Colorful lights';
EnMsg.MIXLY_ke_RGB_A='Common anode RGB';
EnMsg.MIXLY_ke_RGB_B='Common cathode RGB';

EnMsg.MIXLY_ke_BUZZER1='Active buzzer';
EnMsg.MIXLY_ke_BUZZER2='Passive Buzzer';
EnMsg.MIXLY_ke_RELAY='Relay';
EnMsg.MIXLY_ke_v_motor='Vibration motor';
EnMsg.MIXLY_ke_SPEAKER='Speaker';
EnMsg.MIXLY_NOTONE_PIN = 'noTone PIN#';
EnMsg.MIXLY_ke_MOTOR='Fan';
EnMsg.MIXLY_MOTOR_ANALOG='analog value';
EnMsg.MIXLY_ke_MOTOR01='geared motor';
EnMsg.MIXLY_ke_SERVO='servo';
EnMsg.MIXLY_ke_TB6612='TB6612motor';
EnMsg.MIXLY_H='front';
EnMsg.MIXLY_L='back';



EnMsg.MIXLY_RGB_INIT = 'RGB Init';
EnMsg.MIXLY_RGB_SET_BRIGHTNESS = 'RGB Brightness';
EnMsg.MIXLY_RGB_SET_COLOR = 'RGB Color';
EnMsg.MIXLY_RGB_SHOW = 'RGB show';
EnMsg.MIXLY_RGB = 'RGB';
EnMsg.MIXLY_CHASE = 'chase';
EnMsg.MIXLY_RAINBOW = 'rainbow';
EnMsg.MIXLY_RGB_NUM = 'NO';
EnMsg.MIXLY_RGB_COUNT = 'COUNT';
EnMsg.MIXLY_RGB_R = 'R';
EnMsg.MIXLY_RGB_G = 'G';
EnMsg.MIXLY_RGB_B = 'B';
EnMsg.MIXLY_RGBdisplay_rgb_rainbow1 = 'conversion switch time';
EnMsg.MIXLY_RGBdisplay_rgb_rainbow2 = 'cycle switch time';
EnMsg.MIXLY_RGB_DISPLAY_RAINBOW_TYPE_1 = 'Normal';
EnMsg.MIXLY_RGB_DISPLAY_RAINBOW_TYPE_2 = 'Gradient';
EnMsg.MIXLY_RGB_display_rgb_rainbow3 = 'rainbow';

EnMsg.MIXLY_ke_IR_G='PIR Sensor';
EnMsg.MIXLY_ke_FLAME='Flame Sensor';
EnMsg.MIXLY_ke_HALL='Hall Sensor';
EnMsg.MIXLY_ke_CRASH='Crash Sensor';
EnMsg.MIXLY_ke_BUTTON='Button';
EnMsg.MIXLY_ke_sl_BUTTON='Self-locking button';
EnMsg.MIXLY_ke_TUOCH='Capacitive Touch';
EnMsg.MIXLY_ke_KNOCK='Knock Sensor';
EnMsg.MIXLY_ke_TILT='Tilt Sensor';
EnMsg.MIXLY_ke_SHAKE='Vibration Sensor';
EnMsg.MIXLY_ke_REED_S='Reed Switch Sensor';
EnMsg.MIXLY_ke_TRACK='Tracking Sensor';
EnMsg.MIXLY_ke_AVOID='Obstacle Avoidance MSensor';
EnMsg.MIXLY_ke_LIGHT_B='Light Interrupt Sensor';
EnMsg.MIXLY_ke_ROT='Rotation';


EnMsg.MIXLY_ke_ANALOG_T='Analog Temperature Sensor';
EnMsg.MIXLY_ke_SOUND='Sound Sensor';
EnMsg.MIXLY_ke_LIGHT='photocell Sensor';
EnMsg.MIXLY_ke_UV='UV Sensor';
EnMsg.MIXLY_ke_Piezo='Piezo Sensor';
EnMsg.MIXLY_ke_WATER='Water Level Sensor';
EnMsg.MIXLY_ke_SOIL='Soil Sensor';
EnMsg.MIXLY_ke_POTENTIOMETER='rotational potentiometer';
EnMsg.MIXLY_ke_LM35='LM35 Temperature Sensor';
EnMsg.MIXLY_ke_SLIDE_POTENTIOMETER='slide potentiometer';
EnMsg.MIXLY_ke_TEMT6000='TEMT6000 Ambient Light';
EnMsg.MIXLY_ke_STEAM='water vapor sensor';
EnMsg.MIXLY_ke_FILM_P='Thin-film Pressure Sensor';
EnMsg.MIXLY_ke_JOYSTICK='Joystick Sensor';
EnMsg.MIXLY_ke_JOYSTICK_btn='Joystick_button';
EnMsg.MIXLY_ke_SMOKE_DATA='Smoke Sensor digital';
EnMsg.MIXLY_ke_SMOKE_ANALOG='Smoke Sensor analog';
EnMsg.MIXLY_ke_ALCOHOL='Alcohol Sensor';
EnMsg.MIXLY_ke_Voltage='Voltage Sensor';
EnMsg.MIXLY_ke_Current='Current Sensor';


EnMsg.MIXLY_ke_18B20='18B20 Temperature Sensor';
EnMsg.MIXLY_ke_18B20_R='Getting temperature';
EnMsg.MIXLY_ke_DHT11='temperature and humidity Sensor';
EnMsg.MIXLY_DHT11_H='getTemperature';    /////////////
EnMsg.MIXLY_DHT11_T='getHumidity';     ////////////
EnMsg.MIXLY_ke_BMP180='BMP180 altimeter Sensor';
EnMsg.MIXLY_ke_BMP180_T='temperature';
EnMsg.MIXLY_ke_BMP180_A='atmosphere';
EnMsg.MIXLY_ke_BMP180_H='height above sea level ';

EnMsg.MIXLY_ke_BMP280='BMP280 altimeter Sensor';
EnMsg.MIXLY_ke_BMP280_T='temperature';
EnMsg.MIXLY_ke_BMP280_A='atmosphere';
EnMsg.MIXLY_ke_BMP280_H='height above sea level';

EnMsg.MIXLY_ke_SR04='SR04 Ultrasound Module';
EnMsg.MIXLY_ke_3231='DS3231 clock';

//RTC-DS3231/DS1307
EnMsg.MIXLY_ke_DS3231='DS3231';
EnMsg.MIXLY_ke_DS1307='DS1307';
EnMsg.MIXLY_ke_DS3231_GET_TIME='Get';
EnMsg.MIXLY_ke_DS3231_SET_TIME='SetTime';
EnMsg.MIXLY_ke_DS3231_SET_TIME2='Compiled date and time';
EnMsg.MIXLY_ke_YEAR='Year';
EnMsg.MIXLY_ke_MONTH='Month';
EnMsg.MIXLY_ke_DAY='Day';
EnMsg.MIXLY_ke_HOUR='Hour';
EnMsg.MIXLY_ke_MINUTE='Minute';
EnMsg.MIXLY_ke_SECOND='Second';
EnMsg.MIXLY_ke_DAYOFWEEK='DayOfWeek';

EnMsg.MIXLY_ke_ADXL345='Acceleration Sensor';
EnMsg.MIXLY_ADXL345_X='X-axis acceleration'; ///
EnMsg.MIXLY_ADXL345_Y='Y-axis acceleration'; ///
EnMsg.MIXLY_ADXL345_Z='Z-axis acceleration'; ///
EnMsg.MIXLY_ADXL345_XA='X-axis angle';  ///
EnMsg.MIXLY_ADXL345_YA='Y-axis angle';  ///
EnMsg.MLX90614_TYPE = 'Infrared temperature sensor';
EnMsg.MLX90614_TARGET_OBJECT_TEMP = 'Target object temperature';
EnMsg.MLX90614_AMBIENT_TEMP = 'Ambient temperature';
EnMsg.TCS34725_Get_RGB='TCS347255 Sensor Get Color';
EnMsg.ke_Gesture_APDS='Gesture sensor';



EnMsg.MIXLY_DF_LCD = 'LCD';
EnMsg.MIXLY_LCD_PRINT1 = 'print line1';
EnMsg.MIXLY_LCD_PRINT2 = 'print line2';
EnMsg.MIXLY_LCD_PRINT3 = 'print line3';
EnMsg.MIXLY_LCD_PRINT4 = 'print line4';
EnMsg.MIXLY_LCD_ROW = 'row';
EnMsg.MIXLY_LCD_COLUMN = 'column';
EnMsg.MIXLY_LCD_PRINT = 'print';
EnMsg.MIXLY_LCD_SETCOLOR = 'setColor';
EnMsg.MIXLY_LCD_STAT_CURSOR = 'Cursor';
EnMsg.MIXLY_LCD_STAT_NOCURSOR = 'noCursor';
EnMsg.MIXLY_LCD_STAT_BLINK = 'Blink';
EnMsg.MIXLY_LCD_STAT_NOBLINK = 'noBlink';
EnMsg.MIXLY_LCD_STAT_CLEAR = 'Clear';
EnMsg.MIXLY_LCD_NOBACKLIGHT = 'NoBackLight';
EnMsg.MIXLY_LCD_BACKLIGHT = 'BackLight';
EnMsg.MIXLY_NUMBER = 'number';
EnMsg.MIXLY_ke_MATRIX='8*8 dot matrix';

EnMsg.MIXLY_LCD128_SETUP='128X32 LCD start';  ////////////////
EnMsg.MIXLY_LCD128_CURSOR='128X32 LCD set cursor';
EnMsg.MIXLY_DISPLAY_STRING='128X32 LCD display String';
EnMsg.MIXLY_ke_LCD128_PIXLE='128X32 LCD draw pixel';
EnMsg.MIXLY_ke_LCD128_D='128X32 LCD delete pixel';
EnMsg.MIXLY_LCD128_CLEAR='128X32 LCD Clear';



EnMsg.MIXLY_ke_TM1637='4 digit 8-segment LED digital';
EnMsg.MIXLY_ke_TM1637_C='digit';
EnMsg.MIXLY_ke_TM1637_P='display position';
EnMsg.MIXLY_ke_TM1637_Fill='add 0?';
EnMsg.MIXLY_ke_TM1637_light='brightness 0~7';
EnMsg.MIXLY_ke_TM1637_xy='show or hide';
EnMsg.MIXLY_ke_TM1637_left='left';
EnMsg.MIXLY_ke_TM1637_maohao='colon';
EnMsg.MIXLY_ke_TM1637_right='right';
EnMsg.MIXLY_ke_value='value';

//4 Display
EnMsg.MIXLY_ke_4DIGITDISPLAY='DigitDisplay';
EnMsg.MIXLY_ke_4DIGITDISPLAY_DISPLAYSTRING='displayString';
EnMsg.MIXLY_ke_4DIGITDISPLAY_DISPLAYNUMBER='displayNumber';
EnMsg.MIXLY_ke_4DIGITDISPLAY_NOMBER1='No.';
EnMsg.MIXLY_ke_4DIGITDISPLAY_NOMBER2='';
EnMsg.MIXLY_ke_4DIGITDISPLAY_DOT='Dot';
EnMsg.MIXLY_ke_4DIGITDISPLAY_BRIGHTNESS='Brightness';
EnMsg.MIXLY_ke_4DIGITDISPLAY_BRIGHTNESS_RANGE='(1~8)';
EnMsg.MIXLY_ke_4DIGITDISPLAY_TIME_HOUR='Hour';
EnMsg.MIXLY_ke_4DIGITDISPLAY_TIME_MINUTE='Minute';
EnMsg.MIXLY_ke_4DIGITDISPLAY_TIME_SECOND='Second';
//4 Display time
EnMsg.MIXLY_ke_TIME_SEC_ON ='on';
EnMsg.MIXLY_ke_TIME_SEC_BLINK ='blink';
EnMsg.MIXLY_ke_TIME_SEC_OFF='off';


EnMsg.MIXLY_ke_IR_E='Infrared Transmitter Module';
EnMsg.MIXLY_ke_IR_R='Infrared Receiver Module';
EnMsg.MIXLY_ke_W5100='W5100 Ethernet Module';
EnMsg.MIXLY_rc522_iic_init='RFID RC522_I2C Init';
EnMsg.MIXLY_rc522_iic_read='RFID RC522_I2C Read';
EnMsg.MIXLY_ke_BLUETOOTH='Bluetooth';
EnMsg.MIXLY_ke_read='Received signal';


//EnMsg.MIXLY_ke_kzsc = 'Control output';

EnMsg.MIXLY_ke_Count='count';

EnMsg.MIXLY_ke_YEAR = 'year';
EnMsg.MIXLY_ke_MONTH = 'month';
EnMsg.MIXLY_ke_DAY = 'day';
EnMsg.MIXLY_ke_HOUR = 'hour';
EnMsg.MIXLY_ke_MINUTE = 'minute';
EnMsg.MIXLY_ke_SECOND = 'second';
EnMsg.MIXLY_ke_WEEK = 'week';

EnMsg.MIXLY_ke_angle = 'angle';

EnMsg.kids_Ode_to_joy = "Ode_to_joy";
EnMsg.kids_birthday = "birthday";

EnMsg.kids_tone = "tone";
EnMsg.kids_beat = "beat";
EnMsg.kids_play_tone = "play_tone";
EnMsg.kids_notone = "no_tone";

EnMsg.kids_ADkey = "5 key module";

export const EnCatgories = {};
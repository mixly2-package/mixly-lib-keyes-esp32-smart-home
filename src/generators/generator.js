import { JSFuncs } from 'mixly';

////////////////////模拟输出////////////////////

export const ke_a_Write = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var value_num = generator.valueToCode(this, 'NUM', generator.ORDER_ATOMIC);
    generator.definitions_['include_analogWrite'] = '#include <analogWrite.h>';
    var code = 'analogWrite(' + dropdown_pin + ', ' + value_num + ');\n';
    return code;
};

////////////////////LED////////////////////
export const ke_led = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var dropdown_stat = this.getFieldValue('STAT');
    generator.setups_['setup_output_' + dropdown_pin] = 'pinMode(' + dropdown_pin + ', OUTPUT);';
    var code = 'digitalWrite(' + dropdown_pin + ',' + dropdown_stat + ');\n'
    return code;
};

////////////////////蜂鸣器////////////////////

export const ke_tone01 = function (_, generator) {
    var code = this.getFieldValue('STAT');
    return [code, generator.ORDER_ATOMIC];
};

export const ke_buzzer = function (block, generator) {
    var pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var fre = generator.valueToCode(block, 'FREQUENCY', generator.ORDER_ATOMIC);
    var dur = this.getFieldValue('DURATION');

    generator.definitions_['include_tone_init'] = '#include <ESP32Tone.h>\n';
    generator.setups_[`buzzer_${pin}`] = 'pinMode(' + pin + ', OUTPUT);\n';
    var code = 'tone(' + pin + ', ' + fre + ', ' + dur + ', 0);\n';
    return code;
};

export const ke_music = function (block, generator) {
    var pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var play = this.getFieldValue('play');
    generator.definitions_['include_music_init'] = '#include <ESP32Tone.h>\n#include<musicESP32.h>\n';

    generator.definitions_[`music_${pin}`] = 'music Music(' + pin + ');';

    generator.setups_[`buzzer_${pin}`] = 'pinMode(' + pin + ', OUTPUT);\n';

    var code = '' + play + '\n';
    return code;

};

export const ke_notone = function (block, generator) {
    var pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    generator.setups_[`buzzer_${pin}`] = 'pinMode(' + pin + ', OUTPUT);\n';

    var code = 'noTone(' + pin + ', 0);\n';
    return code;

};

////////////////////电机////////////////////

export const ke_motor2 = function (block, generator) {
    var pin1 = generator.valueToCode(this, 'PIN1', generator.ORDER_ATOMIC);
    var pin2 = generator.valueToCode(this, 'PIN2', generator.ORDER_ATOMIC);
    //const val1 = generator.valueToCode(block, 'SPEED1', generator.ORDER_ATOMIC);
    var state1 = this.getFieldValue('STAT1');
    var val2 = generator.valueToCode(this, 'SPEED2', generator.ORDER_ATOMIC);

    generator.setups_[`ke_motor2`] = 'pinMode(' + pin1 + ', OUTPUT);\n  ledcSetup(5, 1200, 8);\n  ledcAttachPin(' + pin2 + ', 5);\n';

    var code = 'digitalWrite(' + pin1 + ',' + state1 + ');\nledcWrite(5, ' + val2 + ');\n';
    return code;

};



////////////////////舵机////////////////////
export const ke_servo = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var value_degree = generator.valueToCode(this, 'angle', generator.ORDER_ATOMIC);
    //value_degree = value_degree.replace('(','').replace(')','')
    var delay_time = generator.valueToCode(this, 'time', generator.ORDER_ATOMIC) || '0'
    //delay_time = delay_time.replace('(','').replace(')','');

    generator.definitions_['include_ESP32_Servo'] = '#include <ESP32_Servo.h>';
    generator.definitions_['var_servo' + dropdown_pin] = 'Servo servo_' + dropdown_pin + ';';
    generator.setups_['setup_servo_' + dropdown_pin] = 'servo_' + dropdown_pin + '.attach(' + dropdown_pin + ');';

    var code = 'servo_' + dropdown_pin + '.write(' + value_degree + ');\n' + 'delay(' + delay_time + ');\n';
    return code;
};

export const ke_servo_r = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);

    generator.definitions_['include_ESP32_Servo'] = '#include <ESP32_Servo.h>';
    generator.definitions_['var_servo' + dropdown_pin] = 'Servo servo_' + dropdown_pin + ';';
    generator.setups_['setup_servo_' + dropdown_pin] = 'servo_' + dropdown_pin + '.attach(' + dropdown_pin + ');';

    var code = 'servo_' + dropdown_pin + '.read()';
    return [code, generator.ORDER_ATOMIC];
};

//////////////////////////数字传感器////////////////////////////////

export const ke_ir_g = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    generator.setups_['setup_input_' + dropdown_pin] = 'pinMode(' + dropdown_pin + ', INPUT);';
    var code = 'digitalRead(' + dropdown_pin + ')';
    return [code, generator.ORDER_ATOMIC];
};

/////////////////按键传感器////////////////
export const ke_button = ke_ir_g;
/////////////////烟雾数字传感器/////////////////
export const ke_smoke_D = ke_ir_g;

//////////////////////模拟传感器/////////////////////////
export const ke_sound = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    generator.setups_['setup_input_' + dropdown_pin] = 'pinMode(' + dropdown_pin + ', INPUT);';
    var code = 'analogRead(' + dropdown_pin + ')';
    return [code, generator.ORDER_ATOMIC];
};

////////////////////水蒸气////////////////////
///
export const ke_steam = ke_sound;



////////////////////烟雾////////////////////

export const ke_smoke = ke_sound;


//////////////////////////DHT11///////////////////////////


export const ke_DHT = function (_, generator) {
    var sensor_type = this.getFieldValue('TYPE');
    var dropdown_pin = this.getFieldValue('PIN');
    var what = this.getFieldValue('WHAT');
    generator.definitions_['include_DHT'] = '#include <DHT.h>';
    generator.definitions_['var_declare_dht' + dropdown_pin] = 'DHT dht' + dropdown_pin + '(' + dropdown_pin + ', ' + sensor_type + ');'
    generator.setups_['DHT_SETUP' + dropdown_pin] = ' dht' + dropdown_pin + '.begin();';
    var code;
    if (what == "temperature")
        code = 'dht' + dropdown_pin + '.readTemperature()'
    else
        code = 'dht' + dropdown_pin + '.readHumidity()'
    return [code, generator.ORDER_ATOMIC];
}
////////////////////////////////////////////////////
//////////////////////////显示屏///////////////////
//////////////////////////////////////////////////////////

///////////////////////////RGB灯/////////////////
export const RGB_color_seclet = function (_, generator) {
    var colour = this.getFieldValue('COLOR');
    colour = '0x' + colour.substring(1, colour.length);
    return [colour, generator.ORDER_NONE];
};

export const RGB_color_rgb = function (_, generator) {
    var R = generator.valueToCode(this, 'R', generator.ORDER_ATOMIC);
    var G = generator.valueToCode(this, 'G', generator.ORDER_ATOMIC);
    var B = generator.valueToCode(this, 'B', generator.ORDER_ATOMIC);
    var colour = "((" + R + " & 0xffffff) << 16) | ((" + G + " & 0xffffff) << 8) | " + B;
    return [colour, generator.ORDER_NONE];
};

export const display_rgb_init = function (_, generator) {
    var dropdown_rgbpin = this.getFieldValue('PIN');
    var type = this.getFieldValue('TYPE');
    var value_ledcount = generator.valueToCode(this, 'LEDCOUNT', generator.ORDER_ATOMIC);
    generator.definitions_['include_Adafruit_NeoPixel'] = '#include <Adafruit_NeoPixel.h>';
    generator.definitions_['var_declare_rgb_display' + dropdown_rgbpin] = 'Adafruit_NeoPixel rgb_display_' + dropdown_rgbpin + ' = Adafruit_NeoPixel(' + value_ledcount + ',' + dropdown_rgbpin + ',' + type + ' + NEO_KHZ800);';
    generator.setups_['setup_rgb_display_begin_' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.begin();';
    return '';
};

export const display_rgb_Brightness = function (_, generator) {
    var dropdown_rgbpin = this.getFieldValue('PIN');
    var Brightness = generator.valueToCode(this, 'Brightness', generator.ORDER_ATOMIC);
    generator.definitions_['include_Adafruit_NeoPixel'] = '#include <Adafruit_NeoPixel.h>';
    generator.setups_['setup_rgb_display_begin_' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.begin();';
    var code = 'rgb_display_' + dropdown_rgbpin + '.setBrightness(' + Brightness + ');\n';
    return code;
};

export const display_rgb = function (_, generator) {
    var dropdown_rgbpin = this.getFieldValue('PIN');
    var value_led = generator.valueToCode(this, '_LED_', generator.ORDER_ATOMIC);
    var COLOR = generator.valueToCode(this, 'COLOR', generator.ORDER_ATOMIC);
    COLOR = COLOR.replace(/#/g, "0x");
    var code = 'rgb_display_' + dropdown_rgbpin + '.setPixelColor((' + value_led + ')-1, ' + COLOR + ');\n';
    return code;
};

export const RGB_color_HSV = function (_, generator) {
    var dropdown_rgbpin = this.getFieldValue('PIN');
    var value_led = generator.valueToCode(this, '_LED_', generator.ORDER_ATOMIC);
    var H = generator.valueToCode(this, 'H', generator.ORDER_ATOMIC);
    var S = generator.valueToCode(this, 'S', generator.ORDER_ATOMIC);
    var V = generator.valueToCode(this, 'V', generator.ORDER_ATOMIC);
    var code = 'rgb_display_' + dropdown_rgbpin + '.setPixelColor((' + value_led + ')-1, ' + 'rgb_display_' + dropdown_rgbpin + '.ColorHSV(' + H + ',' + S + ',' + V + '));\n';
    return code;
};

export const display_rgb_show = function () {
    var board_type = JSFuncs.getPlatform();
    var dropdown_rgbpin = this.getFieldValue('PIN');
    var code = 'rgb_display_' + dropdown_rgbpin + '.show();\n';
    if (board_type.match(RegExp(/ESP32/))) {
        code += 'rgb_display_' + dropdown_rgbpin + '.show();\n';
    }
    return code;
};

export const display_rgb_rainbow1 = function (_, generator) {
    var dropdown_rgbpin = this.getFieldValue('PIN');
    var wait_time = generator.valueToCode(this, 'WAIT', generator.ORDER_ATOMIC);
    generator.setups_['setup_rgb_display_begin_' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.begin();\n';
    var funcName2 = 'Wheel';
    var code2 = 'uint32_t Wheel(byte WheelPos){\n'
        + '  if(WheelPos < 85){\n'
        + '    return rgb_display_' + dropdown_rgbpin + '.Color(WheelPos * 3, 255 - WheelPos * 3, 0);\n'
        + '  }\n'
        + '  else if(WheelPos < 170){\n'
        + '    WheelPos -= 85;\n'
        + '    return rgb_display_' + dropdown_rgbpin + '.Color(255 - WheelPos * 3, 0, WheelPos * 3);\n'
        + '  }\n '
        + '  else{\n'
        + '    WheelPos -= 170;\n'
        + '    return rgb_display_' + dropdown_rgbpin + '.Color(0, WheelPos * 3, 255 - WheelPos * 3);\n'
        + '  }\n'
        + '}\n';
    generator.definitions_[funcName2] = code2;
    var funcName3 = 'rainbow';
    var code3 = 'void rainbow(uint8_t wait){\n'
        + '  uint16_t i, j;\n'
        + '  for(j=0; j<256; j++){\n'
        + '    for(i=0; i<rgb_display_' + dropdown_rgbpin + '.numPixels(); i++){\n'
        + '      rgb_display_' + dropdown_rgbpin + '.setPixelColor(i, Wheel((i+j) & 255));\n'
        + '    }\n'
        + '    rgb_display_' + dropdown_rgbpin + '.show();\n'
        + '    delay(wait);\n'
        + '  }\n'
        + '}\n';
    generator.definitions_[funcName3] = code3;
    var code = 'rainbow(' + wait_time + ');\n'
    return code;
};

export const display_rgb_rainbow2 = function (_, generator) {
    var dropdown_rgbpin = this.getFieldValue('PIN');
    var wait_time = generator.valueToCode(this, 'WAIT', generator.ORDER_ATOMIC);
    var funcName2 = 'Wheel';
    var code2 = 'uint32_t Wheel(byte WheelPos){\n'
        + '  if(WheelPos < 85){\n'
        + '    return rgb_display_' + dropdown_rgbpin + '.Color(WheelPos * 3, 255 - WheelPos * 3, 0);\n'
        + '  }\n'
        + '  else if(WheelPos < 170){\n'
        + '    WheelPos -= 85;\n'
        + '    return rgb_display_' + dropdown_rgbpin + '.Color(255 - WheelPos * 3, 0, WheelPos * 3);\n'
        + '  }\n'
        + '  else{\n'
        + '    WheelPos -= 170;\n'
        + '    return rgb_display_' + dropdown_rgbpin + '.Color(0, WheelPos * 3, 255 - WheelPos * 3);\n'
        + '  }\n'
        + '}\n';
    generator.definitions_[funcName2] = code2;
    var funcName3 = 'rainbow';
    var code3 = 'void rainbow(uint8_t wait){\n'
        + '  uint16_t i, j;\n'
        + '  for(j=0; j<256; j++){\n'
        + '    for(i=0; i<rgb_display_' + dropdown_rgbpin + '.numPixels(); i++){\n'
        + '      rgb_display_' + dropdown_rgbpin + '.setPixelColor(i, Wheel((i+j) & 255));\n'
        + '    }\n'
        + '    rgb_display_' + dropdown_rgbpin + '.show();\n'
        + '    delay(wait);\n'
        + '  }\n'
        + '}\n';
    generator.definitions_[funcName3] = code3;
    var funcName4 = 'rainbowCycle';
    var code4 = 'void rainbowCycle(uint8_t wait){\n'
        + '  uint16_t i, j;\n'
        + '  for(j=0; j<256*5; j++){\n'
        + '    for(i=0; i< rgb_display_' + dropdown_rgbpin + '.numPixels(); i++){\n'
        + '      rgb_display_' + dropdown_rgbpin + '.setPixelColor(i, Wheel(((i * 256 / rgb_display_' + dropdown_rgbpin + '.numPixels()) + j) & 255));\n'
        + '    }\n'
        + '    rgb_display_' + dropdown_rgbpin + '.show();\n'
        + '    delay(wait);\n'
        + '  }\n'
        + '}\n';
    generator.definitions_[funcName4] = code4;
    var code = 'rainbowCycle(' + wait_time + ');\n'
    return code;
};

export const display_rgb_rainbow3 = function (_, generator) {
    var dropdown_rgbpin = this.getFieldValue('PIN');
    var rainbow_color = generator.valueToCode(this, 'rainbow_color', generator.ORDER_ATOMIC);
    var type = this.getFieldValue('TYPE');
    var funcName2 = 'Wheel';
    var code2 = 'uint32_t Wheel(byte WheelPos){\n'
        + '  if(WheelPos < 85){\n'
        + '    return rgb_display_' + dropdown_rgbpin + '.Color(WheelPos * 3, 255 - WheelPos * 3, 0);\n'
        + '  }\n'
        + '  else if(WheelPos < 170){\n'
        + '    WheelPos -= 85;\n'
        + '    return rgb_display_' + dropdown_rgbpin + '.Color(255 - WheelPos * 3, 0, WheelPos * 3);\n'
        + '  }\n'
        + '  else{\n'
        + '    WheelPos -= 170;return rgb_display_' + dropdown_rgbpin + '.Color(0, WheelPos * 3, 255 - WheelPos * 3);\n'
        + '  }\n'
        + '}\n';
    generator.definitions_[funcName2] = code2;
    var code3 = '';
    if (type == "normal")
        code3 = 'for(int RGB_RAINBOW_i = 0; RGB_RAINBOW_i < rgb_display_' + dropdown_rgbpin + '.numPixels(); RGB_RAINBOW_i++){\n'
            + '  rgb_display_' + dropdown_rgbpin + '.setPixelColor(RGB_RAINBOW_i, Wheel(' + rainbow_color + ' & 255));\n'
            + '}\n'
            + 'rgb_display_' + dropdown_rgbpin + '.show();\n';
    else
        code3 = 'for(int RGB_RAINBOW_i = 0; RGB_RAINBOW_i < rgb_display_' + dropdown_rgbpin + '.numPixels(); RGB_RAINBOW_i++){\n'
            + '  rgb_display_' + dropdown_rgbpin + '.setPixelColor(RGB_RAINBOW_i, Wheel(((RGB_RAINBOW_i * 256 / rgb_display_' + dropdown_rgbpin + '.numPixels()) + ' + rainbow_color + ') & 255));\n'
            + '}\n'
            + 'rgb_display_' + dropdown_rgbpin + '.show();\n';
    return code3;
};


////////////////////////////////////lcd 1602////////////////////////////////////

export const group_lcd_init2 = function (_, generator) {
    var varName = this.getFieldValue('VAR');
    var TYPE = this.getFieldValue('TYPE');
    var SCL = this.getFieldValue('SCL');
    var SDA = this.getFieldValue('SDA');
    var board_type = JSFuncs.getPlatform();
    var device = generator.valueToCode(this, 'device', generator.ORDER_ATOMIC) || '0x27';
    if (SDA == "SDA" && SCL == "SCL") {
        generator.definitions_['include_Wire'] = '#include <Wire.h>';
        generator.definitions_['include_LiquidCrystal_I2C'] = '#include <LiquidCrystal_I2C.h>';
        generator.definitions_['var_declare_LiquidCrystal_I2C_' + varName] = 'LiquidCrystal_I2C ' + varName + '(' + device + ',' + TYPE + ');';
    }
    else {
        if (board_type.match(RegExp(/AVR/))) {
            generator.definitions_['include_SoftI2CMaster'] = '#include <SoftI2CMaster.h>';
            generator.definitions_['include_LiquidCrystal_SoftI2C'] = '#include <LiquidCrystal_SoftI2C.h>';
            generator.definitions_['var_declare_LiquidCrystal_SoftI2C_' + varName] = 'LiquidCrystal_SoftI2C ' + varName + '(' + device + ',' + TYPE + ',' + SCL + ',' + SDA + ');';
        }
        else {
            generator.definitions_['include_Wire'] = '#include <Wire.h>';
            generator.definitions_['include_LiquidCrystal_SoftI2C'] = '#include <LiquidCrystal_I2C.h>';
            generator.definitions_['var_declare_LiquidCrystal_I2C_' + varName] = 'LiquidCrystal_I2C ' + varName + '(' + device + ',' + TYPE + ');';
            generator.setups_["setup_Wire"] = 'Wire.begin(' + SDA + ',' + SCL + ');';
        }
    }
    generator.setups_['setup_lcd_init_' + varName] = varName + '.init();';
    generator.setups_['setup_lcd_backlight_' + varName] = varName + '.backlight();';
    return '';
};



export const group_lcd_print = function (_, generator) {
    var varName = this.getFieldValue('VAR');
    var str1 = generator.valueToCode(this, 'TEXT', generator.ORDER_ATOMIC) || '""';
    var str2 = generator.valueToCode(this, 'TEXT2', generator.ORDER_ATOMIC) || '""';

    var code = varName + '.setCursor(0, 0);\n'
    code += varName + '.print(' + str1 + ');\n';
    code += varName + '.setCursor(0, 1);\n';
    code += varName + '.print(' + str2 + ');\n';
    //code+=varName+'.setCursor(0, 2);\n';
    //code+=varName+'.print('+str3+');\n';
    //code+=varName+'.setCursor(0, 3);\n';
    //code+=varName+'.print('+str4+');\n';
    return code;
};



export const group_lcd_power = function () {
    var varName = this.getFieldValue('VAR');
    var dropdown_stat = this.getFieldValue('STAT');
    var code = varName + '.' + dropdown_stat + '();\n'
    return code;
};


////////////////////////////////通讯///////////////////////////////




//////////////////////RFID////////////////////////////
export const rc522_i2c_init = function (_, generator) {
    generator.definitions_['1include_rc522_iic_init'] = '#include <Wire.h>\n#include <MFRC522_I2C.h>\nMFRC522 mfrc522(0x28);\nString rfid_str = "";\n';
    generator.definitions_['1include_rc522_iic_data'] = 'String return_rfid_data()\n' +
        '{\n' +
        '  if ( ! mfrc522.PICC_IsNewCardPresent() || ! mfrc522.PICC_ReadCardSerial() ) {\n' +
        '    delay(50);\n' +
        '    return "0";\n' +
        '  }\n' +
        '  rfid_str = "";\n' +
        '  for (byte i = 0; i < mfrc522.uid.size; i++) {\n' +
        '    rfid_str = rfid_str + String(mfrc522.uid.uidByte[i],HEX);\n' +
        '  }\n' +
        '  return rfid_str;\n' +
        '}\n';

    generator.setups_['1setup_rc522_iic'] = 'Wire.begin();\nmfrc522.PCD_Init();\n';

    return '';
};


export const rc522_i2c_read = function (_, generator) {
    return ['return_rfid_data()', generator.ORDER_ATOMIC];
};


/////////////////////////////////////蓝牙////////////////////////////////////
export const ke_bluetooth = function (_, generator) {
    var val = this.getFieldValue('VAL');
    var branch = generator.statementToCode(this, 'DO');
    var dropdown_pin1 = generator.valueToCode(this, 'PIN1', generator.ORDER_ATOMIC);
    var dropdown_pin2 = generator.valueToCode(this, 'PIN2', generator.ORDER_ATOMIC);


    generator.definitions_['include_Soft'] = '#include <SoftwareSerial.h>\n';
    generator.definitions_['mySerial'] = 'SoftwareSerial mySerial(' + dropdown_pin1 + ', ' + dropdown_pin2 + ');\n';
    generator.definitions_['char'] = 'char ' + val + ';\n';

    generator.setups_['mySerial23'] = 'mySerial.begin(9600);';

    var code = 'if (mySerial.available())\n{\n' + val + ' = mySerial.read();\n';
    code += branch;
    code += '}\n';
    return code;
};

/////////////////////////////////////WIFI////////////////////////////////////
export const wifi_init = function (block, generator) {
    var ssid = generator.valueToCode(block, 'SSID', generator.ORDER_ATOMIC);
    var passwd = generator.valueToCode(block, 'PASSWD', generator.ORDER_ATOMIC);
    generator.definitions_['wifi_init'] = '#include <WiFi.h>\n#include <ESPmDNS.h>\n#include <WiFiClient.h>\n';

    generator.definitions_['wifi_init2'] = 'const char* ssid = ' + ssid + ';\nconst char* password = ' + passwd + ';\nWiFiServer server(80);\n';
    generator.setups_['wifi_setup'] = 'Serial.begin(115200);\n   WiFi.begin(ssid, password);\n   while (WiFi.status() != WL_CONNECTED) {\n   delay(500);\n   Serial.print(".");\n    }\n    Serial.println("");\n    Serial.print("Connected to ");\n    Serial.println(ssid);\n    Serial.print("IP address: ");\n    Serial.println(WiFi.localIP());\n    server.begin();\n    Serial.println("TCP server started");\n    MDNS.addService("http", "tcp", 80);\n';

    return `WiFiClient client = server.available();\n    if (!client) {\n        return;\n    }\n    while(client.connected() && !client.available()){\n        delay(1);\n    }\n    String req = client.readStringUntil('\\r');\n    int addr_start = req.indexOf(' ');\n    int addr_end = req.indexOf(' ', addr_start + 1);\n    if (addr_start == -1 || addr_end == -1) {\n        Serial.print("Invalid request: ");\n        Serial.println(req);\n        return;\n    }\nreq = req.substring(addr_start + 1, addr_end);\n`;

};

export const wifi_read = function (_, generator) {

    return [`req`, generator.ORDER_ATOMIC];

};

export const client_print = function () {

    var data = this.getFieldValue('DATA');

    var code = `client.println("${data}");\n`;

    return code;

};
export const dht_print = function () {
    var dropdown_pin = this.getFieldValue('PIN');
    var sata = this.getFieldValue('SATA');
    var code = `client.println(${'dht' + dropdown_pin + sata});\n`;
    return code;
};
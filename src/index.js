import * as Blockly from 'blockly/core';
import { ZhHansMsg, ZhHansCatgories } from './language/zh-hans';
import { ZhHantMsg, ZhHantCatgories } from './language/zh-hant';
import { EnMsg, EnCatgories } from './language/en';
import * as generators from './generators/generator';
import * as blocks from './blocks/block';

// 载入语言文件
Object.assign(Blockly.Lang.ZhHans, ZhHansMsg);
Object.assign(Blockly.Lang.ZhHant, ZhHantMsg);
Object.assign(Blockly.Lang.En, EnMsg);
Object.assign(Blockly.Lang.ZhHans.MSG, ZhHansCatgories);
Object.assign(Blockly.Lang.ZhHant.MSG, ZhHantCatgories);
Object.assign(Blockly.Lang.En.MSG, EnCatgories);

// 载入图形化模块外观定义文件
Object.assign(Blockly.Blocks, blocks);

// 载入图形化模块代码生成定义文件
Object.assign(Blockly.Arduino.forBlock, generators);